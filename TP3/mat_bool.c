/**
 * \file mat_bool.c
 * \brief Manipulation de matrices booléennes carrées
 * \version 1
 * \date lundi 30 septembre 2019, 10:04:11 (UTC+0200)
 * \authors Pierre Rousselin, Antoine Rozenknop, Sophie Toulouse
*/
#include <stdlib.h>
#include <stdio.h>
#include "mat_bool.h"

mat_bool *mat_bool_creer(int n)
{
	mat_bool * res;
	if ((res = malloc(sizeof(mat_bool))) == NULL) {
		return NULL;
	}
	res->n = n;
	if ((res->tab = calloc( (size_t) n * (size_t) n, sizeof(int) )) == NULL){
		free(res);
		return NULL;
	}
	return res;
}

void mat_bool_detruire(mat_bool *m) {
	free(m->tab);
	free(m);
}

int mat_bool_get_n(mat_bool *m) {
	return m->n;
}

int mat_bool_get_coeff(mat_bool *m, int ligne, int col) {
	return m->tab[ligne * m->n + col];
}

void mat_bool_set_coeff(mat_bool *m, int ligne, int col, int val) {
	m->tab[ligne * m->n + col] = val;
	m->tab[col * m->n + ligne] = val;
}

void mat_bool_afficher(mat_bool *m)	{
	int i, j;

	for (i = 0; i < m->n; i++) {
		printf("|");
		for (j = 0; j < m->n; j++)
			printf("%d\t", m->tab[i * m->n + j]);
		printf("|\n");
	}
}
