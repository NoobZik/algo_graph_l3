#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "graphe-4.h"
#include "pcc.h"
/* definir les constantes symboliques
 * TEST1 pour le premier test (Bellman sur petit graphe sans circuit)
 * TEST2 pour le deuxième (Bellman sur petit graphe avec circuit)
 * TEST3 pour le troisième (Bellman sur un très gros graphe sans circuit)
 */
#define TEST1 0b001
#define TEST2 0b010
#define TEST3 0b100

#define TESTS 0b111

static double gen_vals_test3(int,int);

int main() {
	graphe *g;
	struct pcc *p;
	int *ordre_top;
	if ( TESTS & TEST1)
	  {
	    puts("Bellman, premier test, sans circuit :");
	    if (!(g = graphe_creer(7, 1)))
	      return 1;
	    graphe_ajouter_arc(g, 0, 1, 1.);
	    graphe_ajouter_arc(g, 0, 2, 4.);
	    graphe_ajouter_arc(g, 0, 3, -1.);
	    graphe_ajouter_arc(g, 1, 2, 1.);
	    graphe_ajouter_arc(g, 1, 5, -2.);
	    graphe_ajouter_arc(g, 2, 3, 2.);
	    graphe_ajouter_arc(g, 2, 5, 4.);
	    graphe_ajouter_arc(g, 2, 6, 1.);
	    graphe_ajouter_arc(g, 2, 4, 0.);
	    graphe_ajouter_arc(g, 3, 4, 1.);
	    graphe_ajouter_arc(g, 4, 6, 2.);
	    graphe_ajouter_arc(g, 5, 6, -1.);
	    if (!(p = pcc_bellman(g, 0, &ordre_top)))
	      return 1;
	    pcc_ecrire_dot(p, "bellman1.dot");
	    if (ordre_top) {
	      int i;
	      for (i = 0; i < 7; ++i)
		printf("%d ", i);
	      puts("");
	    }
	    graphe_liberer(g);
	    pcc_liberer(p);
	    free(ordre_top);
	  }
	if (TESTS & TEST2)
	  {
	    puts("Bellman, deuxième test, avec circuit :");
	    if (!(g = graphe_creer(7, 1)))
	      return 1;
	    graphe_ajouter_arc(g, 0, 1, 1.);
	    graphe_ajouter_arc(g, 0, 2, 4.);
	    graphe_ajouter_arc(g, 0, 3, -1.);
	    graphe_ajouter_arc(g, 1, 2, 1.);
	    graphe_ajouter_arc(g, 1, 5, -2.);
	    graphe_ajouter_arc(g, 2, 3, 2.);
	    graphe_ajouter_arc(g, 2, 5, 4.);
	    graphe_ajouter_arc(g, 2, 6, 1.);
	    graphe_ajouter_arc(g, 2, 4, 0.);
	    graphe_ajouter_arc(g, 5, 2, 3.);
	    graphe_ajouter_arc(g, 3, 4, 1.);
	    graphe_ajouter_arc(g, 4, 6, 2.);
	    graphe_ajouter_arc(g, 5, 6, -1.);
	    if (!(p = pcc_bellman(g, 0, &ordre_top)))
	      return 1;
	    pcc_ecrire_dot(p, "bellman2.dot");
	    if (ordre_top) {
	      int i;
	      for (i = 0; i < 7; ++i)
		printf("%d ", i);
	      puts("");
	    }
	    graphe_liberer(g);
	    pcc_liberer(p);
	    free(ordre_top);
	  }
	if (TESTS & TEST3)
	  {
#define N 10000
	    printf("Bellman, création d'un graphe aléatoire sans circuit"
		   " à %d sommets\n", N);
	    if (!(g = graphe_creer_depuis_fonction(N, gen_vals_test3, 1)))
	      return 1;

	    printf("Bellman, test de performance sur un graphe aléatoire "
		   "sans circuit à %d sommets et %d arcs\n",
		   graphe_get_n(g), graphe_get_m(g));
	    {
	      clock_t deb = clock();
	      if (!(p = pcc_bellman(g, 0, &ordre_top)))
		return 1;
	      clock_t fin = clock();
	      printf("Fini en %g secondes\n", (double) (fin - deb) / CLOCKS_PER_SEC);
	    }
	    graphe_liberer(g);
	    pcc_liberer(p);
	    free(ordre_top);
	  }
	return 0;
}

static double gen_vals_test3(int v,int w)
{
  if (v>=w || (double)rand() / RAND_MAX > .5) return INFINITY;
  return rand() % 4 - 2 ;
}
