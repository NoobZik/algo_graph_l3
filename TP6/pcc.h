#ifndef PCC_H
#define PCC_H

#include "graphe-4.h"
#define A_CIRCUIT -1
#define A_CIRCUIT_ABSORBANT -1
#define A_COUT_NEGATIF -1
#define INIT 1
#define FINI 0
struct pcc {
	graphe *g;
	int depart;
	int *parents;
	double *potentiels;
	int statut;
};

typedef struct pcc pcc;

pcc *pcc_init(graphe *g, int depart);
void pcc_liberer(pcc *p);

pcc *pcc_bellman(graphe *g, int depart, int **ordre_top);
pcc *pcc_dijkstra(graphe *g, int depart);
pcc *pcc_ford(graphe *g, int depart);
pcc *pcc_bellman_ford1(graphe *g, int depart);
pcc *pcc_bellman_ford2(graphe *g, int depart);

int pcc_ecrire_dot(pcc *p, char *nom_fichier);
#endif /* ifndef PCC_H */
