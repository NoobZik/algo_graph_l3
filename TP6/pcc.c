#include "graphe-4.h"
#include "pcc.h"
#include "pile.h"
#include <stdlib.h>
#include <math.h>
#include <string.h> /* memcpy */
#include <stdio.h>

pcc *pcc_init(graphe *g, int depart)
{
	int n = graphe_get_n(g);
	pcc *p = malloc(sizeof(pcc));
	if (! ( p &&
		(p->potentiels = malloc((size_t)n * sizeof(double))) &&
		(p->parents = malloc((size_t)n * sizeof(int))) ) ) {
		pcc_liberer(p);
		return NULL;
	}
	p->depart = depart;
	for (--n; n >= 0; --n) {
		p->potentiels[n] = INFINITY;
		p->parents[n] = n;
	}
	p->potentiels[depart] = 0;
	p->statut = INIT;
	p->g = g;
	return p;
}


void pcc_liberer(pcc *p)
{
	if (p) {
		free(p->potentiels);
		free(p->parents);
		free(p);
	}
}

#define FMT_POT_FINI "\t%d [xlabel=<<font color=\"blue\">%g</font>>];\n"
#define FMT_POT_INFINI "\t%d [xlabel=<<font color=\"blue\">&infin;</font>>];\n"
int pcc_ecrire_dot(pcc *p, char *nom_fichier)
{
        int u, v, n = graphe_get_n(p->g);
        FILE *f = fopen(nom_fichier, "w");
	double *couts, val;
	msuc *m;
        if (!f) {
		perror("fopen");
		return -1;
	}
	couts = malloc( (size_t) n * sizeof(double));
	if (!couts) {
		perror("malloc");
		return -2;
	}
        fputs("digraph {\n", f);
	fputs("\trankdir=LR;\n", f);
        for (u = 0; u < n; ++u)
	  if (!isfinite(p->potentiels[u]))
	    fprintf(f, FMT_POT_INFINI, u);
	  else
	    fprintf(f, FMT_POT_FINI, u, p->potentiels[u]);
        fputs("\n", f);
        fprintf(f, "\tsubgraph g {\n%s",
		graphe_est_or(p->g) ? "" : "\t\tedge [dir = none]\n");
	for (u = 0; u < n; ++u)
		for (m = graphe_get_prem_msuc(p->g, u); m; m = msuc_suivant(m)) {
			v = msuc_sommet(m);
			val = msuc_valeur(m);
			if (!graphe_est_or(p->g) && v < u) /* arête déjà rencontrée */
				continue;
			if (u != v && p->parents[v] == u) {
				couts[v] = val;
				continue; /* on la tracera avec l'arbo */
			}
			fprintf(f, "\t\t%d -%c %d ",
					u, graphe_est_or(p->g) ? '>' : '-', v);
			fprintf(f, " [label = %.2f]", val);
			fprintf(f, ";\n");
		}
        fputs("\t}\n", f);
        fputs("\tsubgraph arbo {\n\t\tedge [color = red, fontcolor = red]\n", f);
        for (u = 0; u < graphe_get_n(p->g); ++u)
                if (u != p->parents[u])
                        fprintf(f, "\t\t %d -> %d [label = %.2f];\n",
				p->parents[u], u, couts[u]);
        fputs("\t}\n}\n", f);
	free(couts);
        fclose(f);
        return 0;
}


pcc *pcc_bellman(graphe *g, int depart, int **ordre_top)
{
  return NULL;
}


pcc *pcc_ford(graphe *g, int depart)
{
  return NULL;
}

pcc *pcc_bellman_ford1(graphe *g, int depart)
{
  return NULL;
}



pcc *pcc_bellman_ford2(graphe *g, int depart)
{
  return NULL;
}
