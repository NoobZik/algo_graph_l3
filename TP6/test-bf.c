#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "graphe-4.h"
#include "pcc.h"
/* definir les constantes symboliques
 * TEST1 pour le premier test (Bellman-Ford sur petit graphe sans circuit
 * absorbant)
 * TEST2 pour le deuxième (Bellman-Ford sur petit graphe avec circuit absorbant)
 * TEST3 pour le troisième (Bellman-Ford sur un très gros graphe)
 * TEST4 pour le quatrième (Bellman-Ford sur un très gros graphe avec de
 * probables circuits absorbants)
 */
#define TEST1 0b0001
#define TEST2 0b0010
#define TEST3 0b0100
#define TEST4 0b1000

#define TESTS 0b1111

static double gen_vals_test3(int,int);
static double gen_vals_test4(int,int);

int main() {
  graphe *g;
  struct pcc *p;
  if (TESTS & TEST1)
    {
      puts("Bellman-Ford, premier test, sans circuit :");
      if (!(g = graphe_creer(8, 1)))
	return 1;
      graphe_ajouter_arc(g, 0, 1, 2.);
      graphe_ajouter_arc(g, 0, 3, 4.);
      graphe_ajouter_arc(g, 1, 3, -1.);
      graphe_ajouter_arc(g, 1, 5, 5.);
      graphe_ajouter_arc(g, 1, 4, 4.);
      graphe_ajouter_arc(g, 2, 6, 1.);
      graphe_ajouter_arc(g, 2, 0, -3.);
      graphe_ajouter_arc(g, 3, 2, 2.);
      graphe_ajouter_arc(g, 4, 7, 2.);
      graphe_ajouter_arc(g, 5, 6, 3.);
      graphe_ajouter_arc(g, 6, 7, 2.);
      graphe_ajouter_arc(g, 6, 3, -2.);
      graphe_ajouter_arc(g, 7, 5, -3.);
      if (!(p = pcc_bellman_ford2(g, 0)))
	return 1;
      pcc_ecrire_dot(p, "bf1.dot");
      graphe_liberer(g);
      pcc_liberer(p);
    }
  if (TESTS & TEST2)
    {
      puts("Bellman, deuxième test, avec circuit :");
      if (!(g = graphe_creer(8, 1)))
	return 1;
      graphe_ajouter_arc(g, 0, 1, 2.);
      graphe_ajouter_arc(g, 0, 3, 4.);
      graphe_ajouter_arc(g, 1, 3, -1.);
      graphe_ajouter_arc(g, 1, 5, 5.);
      graphe_ajouter_arc(g, 1, 4, -2.);
      graphe_ajouter_arc(g, 2, 6, 1.);
      graphe_ajouter_arc(g, 2, 0, -3.);
      graphe_ajouter_arc(g, 3, 2, 2.);
      graphe_ajouter_arc(g, 4, 7, 2.);
      graphe_ajouter_arc(g, 5, 6, 3.);
      graphe_ajouter_arc(g, 6, 7, 2.);
      graphe_ajouter_arc(g, 6, 3, -2.);
      graphe_ajouter_arc(g, 7, 5, -3.);
      if (!(p = pcc_bellman_ford2(g, 0)))
	return 1;
      pcc_ecrire_dot(p, "bf2.dot");
      if (p->statut == A_CIRCUIT_ABSORBANT)
	puts("Le graphe a un circuit absorbant.");
      graphe_liberer(g);
      pcc_liberer(p);
    }
  if (TESTS & TEST3)
    {
#define N 1000
      if (!(g = graphe_creer_depuis_fonction(N, gen_vals_test3, 1)))
	return 1;
      printf("Bellman-Ford, test de performance sur un graphe aléatoire "
	     "sans poids négatif à %d sommets et %d arcs\n",
	     graphe_get_n(g), graphe_get_m(g));
      
      {
	clock_t deb, fin;
	deb = clock();
	if (!(p = pcc_bellman_ford2(g, 0)))
	  return 1;
	fin = clock();
	printf("Fini en %g secondes\n", (double) (fin - deb) / CLOCKS_PER_SEC);
      }
      graphe_liberer(g);
      pcc_liberer(p);
    }
  if (TESTS & TEST4)
    {
#define N 1000
      if (!(g = graphe_creer_depuis_fonction(N, gen_vals_test4, 1)))
	return 1;
      
      printf("Bellman-Ford, test de performance sur un graphe aléatoire "
	     "avec un probable circuit absorbant à %d sommets et %d arcs\n",
	     graphe_get_n(g), graphe_get_m(g));
      {
	clock_t deb = clock(), fin;
	if (!(p = pcc_bellman_ford2(g, 0)))
	  return 1;
	fin = clock();
	printf("Fini en %g secondes\n", (double) (fin - deb) / CLOCKS_PER_SEC);
      }
      if (p->statut == A_CIRCUIT_ABSORBANT)
	puts("Le graphe a un circuit absorbant.");
      graphe_liberer(g);
      pcc_liberer(p);
    }
  return 0;
}








#ifdef TEST3
static double gen_vals_test3(int v, int w)
{
  if ((double) rand()/RAND_MAX > .1) return INFINITY;
  return 1 + rand() % 4;
}
#endif

#ifdef TEST4
static double gen_vals_test4(int v,int w)
{
  if ((double) rand()/RAND_MAX > .1) return INFINITY;
  return -0.5 + rand() % 4;
}
#endif
