# Algo des graphes TP 1 #

## 2. Fonctions de manipulation ##

*   Question 1 : Donner les valeurs des champs de la structure, si la variable g de type graphe représente le graphe. :

int n = 5 (Le nombre d'aretes)
int m = 7 (Le nombre de sommets)

***Valeurs du tableau du shémas (sans les d)***

|   | 0 | 1 | 2 | 3 | 4 | d |
|---|---|---|---|---|---|---|
| 0 | 0 | 1 | 0 | 0 | 1 | 2 |
| 1 | 1 | 0 | 0 | 0 | 2 | 3 |
| 2 | 0 | 0 | 1 | 1 | 0 | 3 |
| 3 | 0 | 0 | 1 | 0 | 0 | 1 |
| 4 | 1 | 2 | 0 | 0 | 0 | 3 |
| d | 2 | 3 | 3 | 1 | 3 |   |

*   Combien (en nombre d'int) de place en mémoire prend une variable de type graphe ?

Sachant que le macro GRAPHE_ORDRE_MAX est fixé à 9, il y a 81 int au tableau.

On en déduis qu'une structure graphe prends 83 int en mémoire. (Ce qui représente 262 octect).

### 2.1 Initialisation ###

graphe_simple : Un graphe simple est un graphe qui ne contient ni boule, ni arêtes parallèles.

*   Pour traîter les cas d'une boucle : il faut que les cases i == j soit égales à 0.
*   Pour traîter les cas des arêtes parallèles, il faut que les cases i, j soit au maximum 1.

Question 2 : graphe_stable : Un graphe stable est un graphe qui n'a pas d'arêtes.

### Ajout / suppression d'arêtes ###

*   Ajout d'un arête :

Pour ajouter un arête il faut définir les cas suivant :
*   Si u == v, +1 à cette case et + 1 à g->m
*   Si u != v alors on fait +1 à g->m, ensuite +1 à la case u,v et v,u.
