#include <stdio.h>
#include <stdlib.h>
#include "graphe-1.h"

int main(void) {
  graphe g, g_bis;
  graphe complet;
  graphe_stable(&g, 5);
  graphe_ajouter_arete(&g, 0, 1);
  graphe_ajouter_arete(&g, 0, 4);
  graphe_ajouter_arete(&g, 1, 4);
  graphe_ajouter_arete(&g, 1, 4);
  graphe_ajouter_arete(&g, 2, 2);
//  graphe_ajouter_arete(&g, 2, 3);
//  graphe_ajouter_arete(&g, 2, 3);
//  puts("Graphe avant graphe_supprimer_arete");
  graphe_afficher(&g);
  if(graphe_stable(&g_bis, 5) != 0) {
    puts("Erreur de création d'un graphe stable (hors bornes)");
  }
  else {
    puts("Graphe stable initialisé");
    graphe_afficher(&g_bis);
  }

  if(graphe_est_simple(&g) == 0) {
    puts("Le graphe g n'est pas simple");
  }
  else {
    puts("Le graphe g est simple");
  }

  if(graphe_est_simple(&g_bis) == 0) {
    puts("Le graphe g_bis n'est pas simple");
  }
  else {
    puts("Le graphe g_bis est simple");
  }
  /* Suppresions des aretes du graphe 1 pour le rendre graphe simple */
  puts("Suppression de l'arête 2,2 et 4,1 du graphe g");
  graphe_supprimer_arete(&g, 2, 2);
  graphe_supprimer_arete(&g, 4, 1);
  puts("Apres suppression");
  graphe_afficher(&g);
  if(graphe_est_simple(&g) == 0) {
    puts("Le graphe g n'est pas simple");
  }
  else {
    puts("Le graphe g est simple");
  }
/*
  switch (graphe_supprimer_arete(&g, 2, 3)) {
    case -1 : puts("Erreur de suppression"); break;
    case 0 : puts("Arête supprimé"); break;
  }

  switch (graphe_supprimer_arete(&g, 2, 3)) {
    case -1 : puts("Erreur de suppression"); break;
    case 0 : puts("Arête supprimé"); break;
  }

  switch (graphe_supprimer_arete(&g, 2, 3)) {
    case -1 : puts("Erreur de suppression"); break;
    case 0 : puts("Arête supprimé"); break;
  }

  puts("Graphe après graphe_supprimer_arete");
  graphe_afficher(&g);
*/

  if (graphe_complet(&complet, 5) == -1) {
    puts("erreur de generation d'un graphe complet");
  }
  graphe_afficher(&complet);
  return EXIT_SUCCESS;
}
