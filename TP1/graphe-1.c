/* graphe-1 : représentation des graphes non orientés
 * par leur matrice d'adjacence :: fichier source
*/

#include "graphe-1.h"

#include <stdio.h>	/* pour printf() */

/* ______________________________________ Initialisation
*/

/* Cette fonction initialise un graphe en un stable d'ordre n
 * si 0 <= n <= GRAPHE_ORDRE_MAX (auquel cas la fonction retourne 0),
 * elle renvoie -1 sinon
*/
int graphe_stable(graphe* g, int n)
{
  int statut = -1;		/* statut de réussite (statut == 0) ou d'échec (statut == -1) de la fonction */

  if (!g) {
  return statut;
  }
  if (n < 0 || n > GRAPHE_ORDRE_MAX) {
  return statut;
  }

  g->n = n;
  g->m = 0;
  int i = -1;
  int j = -1;

  for(i = 0; i < n; i++) {
  for (j = 0 ; j < n; j++) {
  g->adj[i][j] = 0;
  }
  }
  statut = 0;

  /* retour du statut de réussite ou d'échec (n < 0 ou n > GRAPHE_ORDRE_MAX) */
  return statut;
}

/* ______________________________________ Ajout / Suppression d'arêtes */

/* ajout d'une arête donnée */
void graphe_ajouter_arete(graphe* g, int v, int w)
{
  if (!g)
  return;
  if ((v < 0 || g->n > GRAPHE_ORDRE_MAX))
  return;


  g->adj[v][w] += 1;
  g->m += 1;

  if (v != w)
  g->adj[w][v] += 1;

}

/* suppression d'une arrête donnée
  renvoie 0 si l'arête peut être supprimée et -1 sinon
*/
int graphe_supprimer_arete(graphe* g, int v, int w)
{
  int statut = -1;	/* statut de réussite (statut == 0)
     ou d'échec (statut == -1) de la fonction */

  if(!g) {
  return statut;
  }

  if (g->adj[v][w] == 0) {
  return -1;
  }

  g->adj[v][w] -= 1;

  if (v != w) {
  g->adj[w][v] -= 1;
  }

  /* retour du statut de réussite ou d'échec */
  return statut;
}

/* ______________________________________ Accesseurs en lecture
*/

/* Cette fonction renvoie le nombre de sommets du graphe
*/
int graphe_get_n(graphe* g)
{
  if(!g) {
  return -1;
  }
  return g->n;
}

/* Cette fonction renvoie le nombre d'arêtes du graphe */
int graphe_get_m(graphe* g)
{
  if (!g) {
  return -1;
  }

  return g->m;
}

/* Cette fonction renvoie le nombre d'occurrences d'une arête donnée dans le graphe
*/
int graphe_get_multiplicite_arete(graphe* g, int v, int w)
{
  if (!g) {
  return -1;
  }

  return g->adj[v][w];
}

/* Cette fonction renvoie le degre du sommet v
*/
int graphe_get_degre(graphe* g, int v)
{
  int deg =0;

  int w = -1;

  while (++w < g->n) {
  deg += g->adj[v][w];
  }
  deg += g->adj[v][v];

  return deg;
}

/* Retourne 1 si le graphe est simple, 0 sinon */
int graphe_est_simple(graphe *g)
{


  int u, v;
  for ( u = 0; u < graphe_get_n(g); ++u)
    for (v = u; v < graphe_get_n(g); ++v)
      if (graphe_get_multiplicite_arete(g, u, v) >=  2 ||
      (u == v  && graphe_get_multiplicite_arete(g, u, v)))
        return 0;
  return 1;
}
/*

return 1;
*/
/* ______________________________________ Entrées / Sorties
*/

/**
 * [graphe_degre description]
 * @param  g [description]
 * @param  v [description]
 * @return   [description]
 */
/*int graphe_degre(graphe* g, int v){
    int res =0;
    for(int x=0;x<(g->n);x++){
        if(g->is_or==1){
            res+=
            g->adj[v][x]+g->adj[x][v];
        } else {
            res+=g->adj[v][x];
        }
    }
    printf("\nres = %u",res);
    return res;
}
*/
/**
 * [graphe_complet description]
 * @param  g [description]
 * @param  n [description]
 * @return   [description]
 */
int graphe_complet(graphe *g, int n) {
  if (!g) {
    return -1;
  }
  if (n < 1) {
    return -1;
  }

  int i, j;
  i = j = -1;

  while (++i < n) {
    while (++j < n) {
      g->adj[i][j] = (i == j) ? 0 : 1;
    }
  }
}

/* cette fonction affiche les données d'un graphe g <> NULL et si 0 <= g->n <= GRAPHE_ORDRE_MAX
*/
void graphe_afficher(graphe* g)
{
  int v, w;	/* variables de boucle */

  /* on n'affiche les données que si g <> NULL et l'ordre n du graphe est techniquement correct */
  if(g == NULL)
  printf("Structure non instanciee (NULL)\n");
  else if(0 > g->n || GRAPHE_ORDRE_MAX < g->n)
  printf("Structure incoherente (g->n de valeur %d hors intervalle requis {0 ,.., %d})\n", g->n, GRAPHE_ORDRE_MAX);
  else
  {
  /* __ statistiques / données globales */
  printf("graphe d'ordre %d sur %d aretes: \n",
  graphe_get_n(g), graphe_get_m(g));

  /* __ matrice adjacence */

  /* ligne indices colonnes */
  printf("\t\t");
  for (w = 0 ; w < graphe_get_n(g) ; w ++)
  printf("%d\t", w);

  printf("|d+\n");

  /* ligne séparatrice entête colonne / corps matrice */
  printf("\t\t");
  for (w = 0 ; w < graphe_get_n(g) ; w ++)
  printf("_\t");

  printf("__\n");

  /* lignes de la matrice */
  for (v = 0 ; v < graphe_get_n(g) ; v ++)
  {
  printf("%d\t|\t", v);
  for (w = 0 ; w < graphe_get_n(g) ; w ++)
  printf("%d\t", graphe_get_multiplicite_arete(g, v, w));

  printf("|%d\n", graphe_get_degre(g, v));
  }

  /* ligne séparatrice corps matrice / degrés entrants */
  printf("\t\t");
  for (w = 0 ; w < graphe_get_n(g) ; w ++)
  printf("_\t");

  printf("__\n");

  /* lignes des degrés */
  printf("d-\t|\t");
  for (w = 0 ; w < graphe_get_n(g) ; w ++)
  printf("%d\t", graphe_get_degre(g, w));

  printf("\n");
  }
}
