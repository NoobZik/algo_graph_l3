# TP5 #

## Question 1 ##

| Header One     | Header Two     |
| :------------- | :------------- |
| 1       | (1,1)(2,1)(3,1)    |
| 2       | (0,1)(2,1)(3,1)       |
| 3       | (0,1)(1,2)(2,1)       |

Dans les graphes non orientés, chaque arêtes donne lieu à 2 éléments dans la liste
de successeur sauf les boucles
