/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Question3.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/23 17:38:04 by NoobZik           #+#    #+#             */
/*   Updated: 2019/09/23 18:31:50 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include  <stdio.h>/* fopen , fclose , fprintf , perror , FILE */
#include  <stdlib.h>/*  EXIT_SUCCESS , EXIT_FAILURE  */
#include "graphe-1.h"

int main(void) {
  FILE *f;
  //int i,j;
  char  nom_fichier [] = "Q2.dot";

  graphe g;
  graphe_stable(&g, 5);
  graphe_ajouter_arete(&g, 0, 1);
  graphe_ajouter_arete(&g, 0, 4);
  graphe_ajouter_arete(&g, 1, 4);
  graphe_ajouter_arete(&g, 1, 4);
  graphe_ajouter_arete(&g, 2, 2);
  graphe_ajouter_arete(&g, 2, 3);

  graphe_afficher(&g);

  f = fopen(nom_fichier , "w");

  if (f == NULL) {
    perror("fopen");
    /* écrit la  dernière  erreur  rencontrée par  fopen */
    return  EXIT_FAILURE;
  }

  graphe_ecrire_dot(&g, nom_fichier);
/*
  fprintf(f, "graph\n {");

  for (i = 0; i < 10; i++)
    fprintf(f, "%d;\n", i);

  for(i = 0; i < 10; i++) {
    for(j=i+1; j < 10; j++) {
      fprintf(f, "%d -- %d;\n", i, j);
    }
  }
  fprintf(f, "}");
  fclose(f);
*/
  system("dot -Tpdf Q2.dot -o mon_graphe.pdf");
  return  EXIT_SUCCESS;
}
