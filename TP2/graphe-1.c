/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   graphe-1.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/17 12:57:29 by NoobZik           #+#    #+#             */
/*   Updated: 2019/09/25 14:54:19 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/* graphe-1 : représentation des graphes non orientés
 * par leur matrice d'adjacence :: fichier source
*/

#include "graphe-1.h"
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>	/* pour printf() */
/* ______________________________________ Initialisation
*/

/* Cette fonction initialise un graphe en un stable d'ordre n
 * si 0 <= n <= GRAPHE_ORDRE_MAX (auquel cas la fonction retourne 0),
 * elle renvoie -1 sinon
*/
int graphe_stable(graphe* g, int n) {
  /* statut de réussite (statut == 0) ou d'échec (statut == -1) de la fonction */

  /* Vérification de l'existence des paramètre (prog défensive) */

  if (!g)
    return -1;
  if (n > GRAPHE_ORDRE_MAX)
    return -1;

  /* Affectation du nombre de sommets */

  g->n = n;

  /* Initialisation d'un graphe stable (par définition, aucun arêtes)*/
  g->m = 0;
  int i = -1, j = -1;
  while (++i < n) {
    while (++j < n) {
      g->adj[i][j] = 0;
    }
    j = -1;
  }
  /* Condition de retour toujours vrai */
  return (i == n) ? 0 : -1;
  /* retour du statut de réussite ou d'échec (n < 0 ou n > GRAPHE_ORDRE_MAX) */

}

/* ______________________________________ Ajout / Suppression d'arêtes */

/* ajout d'une arête donnée */
void graphe_ajouter_arete(graphe* g, int v, int w)  {
  /* progra defensive */
  if (!g)
    return;
  if (v > g->n || w > g->n)
    return;

  g->m += 1;
  if (v == w) {
    g->adj[v][w] += 1;
  }
  else {
    g->adj[v][w] += 1;
    g->adj[w][v] += 1;
  }
}

/* suppression d'une arête donnée
  renvoie 0 si l'arête peut être supprimée et -1 sinon
*/
int graphe_supprimer_arete(graphe* g, int v, int w) {
  int statut = -1;	/* statut de réussite (statut == 0)
     ou d'échec (statut == -1) de la fonction */

  if (!g)
    return -1;
  if (v > g->n || w > g->n)
    return -1;

  if (v == w) {
    if (g->adj[v][w]-1 >= 0) {
      g->adj[v][w] -= 1;
      g->m -= 1;
    }
    statut = 0;
  }

  else if (g->adj[v][w]-1 >= 0) {
    g->adj[v][w] -= 1;
    g->adj[w][v] -= 1;
    g->m -=1;
    statut = 0;
  }

  /* retour du statut de réussite ou d'échec */
  return statut;
}

/* ______________________________________ Accesseurs en lecture
*/

/* Cette fonction renvoie le nombre de sommets du graphe
*/
int graphe_get_n(graphe* g) {
    return (!g) ? -1 : g->n;
}

/* Cette fonction renvoie le nombre d'arêtes du graphe */
int graphe_get_m(graphe* g) {
  return (!g) ? -1 : g->m;

}

/* Cette fonction renvoie le nombre d'occurrences d'une arête donnée dans le graphe
*/
int graphe_get_multiplicite_arete(graphe* g, int v, int w)  {
  if (!g)
    return -1;
  if (v > g->n || w > g->n)
    return -1;

  return g->adj[v][w];
}

/* Cette fonction renvoie le degre du sommet v
*/
int graphe_get_degre(graphe* g, int v) {
  int deg =0;
  if (!g) return -1;
  if (v >= g->n) return -1;
  /* TODO */

  int i = -1;

  while (++i < g->n) {
    deg += ((i == v) && g->adj[i][v] > 0) ? 2 : g->adj[v][i];
  }

  return deg;
}

/* Retourne 1 si le graphe est simple, 0 sinon */
int graphe_est_simple(graphe *g)  {
  /*int u, v;
  for ( u = 0; u < graphe_get_n(g); u++)
    for (v = u; v < graphe_get_n(g); v++)
      if (graphe_get_multiplicite_arete(g, u, v) >=  2 ||
      (u == v  && graphe_get_multiplicite_arete(g, u, v)))
        return 0;
  return 1;*/
  if (!g) return -1;
  int i = -1;
  int j = -1;

  while (++i < g->n) {
    while (++j < g->n) {
      if (i == j) {
        // cas ou i == j, du coup pas de boucle
        if (g->adj[i][j] != 0)
          return 0;
      }
      // Cas où i != j, on regarde les aretes adjacents + autres aretes qui link
      //d'autres sommets
      else if (g->adj[i][j] > 1) {
        return 0;
      }
    }
    j = -1;
  }
  return 1;
}

/**
 * Initialise un graphe complet, chaque pair de sommet à une arrete
 * un sommet ne peut pas avoir une arete sur lui meme (boucle).
 * Du coup la diagonal est 0. et les autre c'est 1.
 * @param  g Structure d'un graphe
 * @param  n Le nombre de sommets à mettre dans ce graphe
 * @return   -1 si le nombre de sommets est trop grand
 *            0 dans le cas contraire.
 */
int graphe_complet(graphe *g, int n) {
  if (!g) return -1;
  if (n > GRAPHE_ORDRE_MAX) return -1;

  g->n = n;
  g->m = 0;
  int i, j;
  i = j = -1;
  while (++i < g->n) {
    j = i - 1;
    while (++j < g->n) {
      if (i == j) {
        g->adj[i][j] = 0;
      }
      else {
        g->adj[i][j] = 1;
        g->adj[j][i] = 1;
        g->m += 1;
      }
    }
  }
  return 0;
}
/**
 * Question 6
 * [graphe_aleatoire description]
 * @param  g [description]
 * @param  n nombre de sommets aléatoire
 * @param  p probabilité entre 0 et 1
 * @return   [description]
 */
int graphe_aleatoire(graphe *g, int n, double p) {
  if (!g) return -1;
  if (n > GRAPHE_ORDRE_MAX) return -1;
  if (p > 1 || p < 0) return -2;

  // Initialisation d'un gaphe stable
  if (graphe_stable(g, n) < 0)
    return -1;
  int i, j;
  for (i = 0; i < g->n; i++) {
    for(j = i + 1; j < g->n; j++) {
      if ((double) rand() / RAND_MAX < p) {
        graphe_ajouter_arete(g, i, j);
      }
    }
  }

  return 0;
}

/**
 * Question 7
 * [graphe_cycle description]
 * @param  g [description]
 * @param  n [description]
 * @return   [description]
 */
int graphe_cycle(graphe *g, int n) {
  if (!g) return -1;
  if (n > GRAPHE_ORDRE_MAX) return -1;
  if (graphe_stable(g, n) < 0) return -1;

  int i, j;
  i = -1;

  // Initialisation successives de [0,1] [1,2] ... [n-1, n]
  while (++i < n) {
    j = i + 1;
    graphe_ajouter_arete(g, i, j);
  }
  return 0;
}
/**
 * Question 8
 * [graphe_bipartie description]
 * @param  g [description]
 * @param  m Cardinal de W
 * @param  p Cardinal de Z
 * @return   [description]
 */
int graphe_biparti(graphe *g, int m, int p) {
  if (!g) return -1;
  if (m > GRAPHE_ORDRE_MAX || p > GRAPHE_ORDRE_MAX) return -1;

  // Initialisation d'un graphe stable avec le plus grand des deux, m et p
  graphe_stable(g,(m > p) ? m : p);

  return 0;
}
/* ______________________________________ Entrées / Sorties
*/

/* cette fonction affiche les données d'un graphe g <> NULL et si 0 <= g->n <= GRAPHE_ORDRE_MAX
*/
void graphe_afficher(graphe* g)
{
  int v, w;	/* variables de boucle */

  /* on n'affiche les données que si g <> NULL et l'ordre n du graphe est techniquement correct */
  if(g == NULL)
  printf("Structure non instanciee (NULL)\n");
  else if(0 > g->n || GRAPHE_ORDRE_MAX < g->n)
  printf("Structure incoherente (g->n de valeur %d hors intervalle requis {0 ,.., %d})\n", g->n, GRAPHE_ORDRE_MAX);
  else
  {
  /* __ statistiques / données globales */
  printf("graphe d'ordre %d sur %d aretes: \n",
  graphe_get_n(g), graphe_get_m(g));

  /* __ matrice adjacence */

  /* ligne indices colonnes */
  printf("\t\t");
  for (w = 0 ; w < graphe_get_n(g) ; w ++)
  printf("%d\t", w);

  printf("|d+\n");

  /* ligne séparatrice entête colonne / corps matrice */
  printf("\t\t");
  for (w = 0 ; w < graphe_get_n(g) ; w ++)
  printf("_\t");

  printf("__\n");

  /* lignes de la matrice */
  for (v = 0 ; v < graphe_get_n(g) ; v ++)
  {
  printf("%d\t|\t", v);
  for (w = 0 ; w < graphe_get_n(g) ; w ++)
  printf("%d\t", graphe_get_multiplicite_arete(g, v, w));

  printf("|%d\n", graphe_get_degre(g, v));
  }

  /* ligne séparatrice corps matrice / degrés entrants */
  printf("\t\t");
  for (w = 0 ; w < graphe_get_n(g) ; w ++)
  printf("_\t");

  printf("__\n");

  /* lignes des degrés */
  printf("d-\t|\t");
  for (w = 0 ; w < graphe_get_n(g) ; w ++)
  printf("%d\t", graphe_get_degre(g, w));

  printf("\n");
  }
}
/**
 * [graphe_ecrire_dot description]
 * @param  g           [description]
 * @param  nom_fichier [description]
 * @return             [description]
 */
int  graphe_ecrire_dot(graphe *g, char *nom_fichier) {
  if (!g) return -1;
  if (!nom_fichier)return -1;
  int i,j,k;
  int sommets = g->n;
  FILE *f;
  f = fopen(nom_fichier , "w");

  if (f == NULL) {
    perror("fopen");
    /* écrit la  dernière  erreur  rencontrée par  fopen */
    return  EXIT_FAILURE;
  }
  fprintf(f, "graph {\n");
  /* listes des sommets */
  for (i = 0; i < sommets; i++) {
    fprintf(f, "  %d;\n", i);
  }
  fprintf(f, "\n\n");

  for (i = 0; i < sommets; i++) {
    for (j = i; j < sommets  ;j++) {
      /* Arete simple */
      if (g->adj[i][j] == 1) {
        fprintf(f, "  %d -- %d;\n", i, j);
      }
      /* Aretes adjacentes */
      if (g->adj[i][j] > 1) {
        for (k = 0; k < g->adj[i][j]; k++) {
          fprintf(f, "  %d -- %d;\n", i, j);
        }
      }
    }
  }
  fprintf(f, "}\n");
  fclose(f);
  return 0;
}
/**
 * [graphe_charger_dot description]
 * @param  g           [description]
 * @param  nom_fichier [description]
 * @return             [description]
 */
int graphe_charger_dot(graphe *g, char *nom_fichier) {
  FILE * f;
  int n = 0;
  int a, b;
  int i;
  char temp[500];
  if ((f = fopen(nom_fichier, "r")) == 0) {
    puts(nom_fichier);
    puts("Erreur d'ouverture du fichier");
    return -1;
  }
  char buffer[500];

  fgets(buffer, sizeof(buffer),f);
;
  if (strcmp(buffer, "graph {\n") != 0) {
    puts("Erreur de compatibilité de fichier");
    puts(nom_fichier);
    return -2;
  }
  puts("Fichier ouvert et compatible");
  puts("Lecture du buffer 1");

  while (fgets(buffer, sizeof(buffer), f) && buffer[0] != '\n') {
    n += 1;
  }

  graphe_stable(g, n);

  while (fgets(buffer, sizeof(buffer), f) && buffer[0] != '}') {

    for (i = 0; buffer[i] != '\0'; i++) {
      if ('0' <= buffer[i] && buffer[i] <= '9')
        strcat(temp, &buffer[i]);

      else if (buffer[i] == '-' && buffer[i+1] == '-') {
        a = atoi(&buffer[0]);
      }

      else if (buffer[i] == '-' && buffer[i+1] == ' ') {
        b = atoi(&buffer[i+2]);
      }

    }
    graphe_ajouter_arete(g, a, b);
  }

  fclose(f);

  return 0;
}
