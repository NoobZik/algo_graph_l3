/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main-dessin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/24 14:32:16 by NoobZik           #+#    #+#             */
/*   Updated: 2019/09/25 13:25:12 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "graphe-1.h"

int main(int argc, char const *argv[]) {
  (void) argc;
  (void) argv;


  srand( (unsigned int) time(NULL));
//  double r = .4;

  graphe g;
  graphe stand;
  /*if (graphe_aleatoire(&g, 9, r) != 0) {
    puts("Erreur aleatoire");
  }
  graphe_afficher(&g);
*/
  graphe_stable(&g, 5);
  graphe_ajouter_arete(&g,0,1);
  graphe_ajouter_arete(&g,2,2);
  graphe_ajouter_arete(&g,3,2);
  graphe_ajouter_arete(&g,4,1);
  graphe_ajouter_arete(&g,0,4);
  graphe_ajouter_arete(&g,4,1);

  puts("Graphe du dot a faire charger");
  graphe_afficher(&g);

  char nom_de_fichier[] = "./biatch.dot";
  puts("Ouverture du fichier en cours...");

  graphe_charger_dot(&g, nom_de_fichier);
  graphe_afficher(&g);

//  system("dot -Tpdf biatch.dot -o mon_graphe.pdf");

  return 0;
}
